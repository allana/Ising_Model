#!/usr/bin/env python3


import numpy as np
import random
import math




kB=1.   # Boltzman Constant J/K
J=1.    # Exchange Energy
h=0

#####################################################################################
#####MAKING THE INITIAL LATTICE#######
def init_spin_array(rows, cols):
    return np.random.choice((-1, 1), size=(rows, cols))
    
def all_spins_up(rows, cols):
    return np.ones((rows, cols))
 
#Given spin Si at x,y in lattice, prints values of nearest neighbours
#in convenion left, right, above, below
def near(g, x ,y,N):
    left=g[y][x-1] if x > 0 else g[y][N-1]
    right=g[y][x+1] if(x) <  (N-1) else g[y][0]
    above=g[y-1][x] if (y) > 0 else g[N-1][x]
    below=g[y+1][x] if (y)<(N-1) else g[0][x]
    
    final = [left,right,above,below] 
    return final

#Energy of lattice in its current state g
def current_energy(g,N):
    energy=0
    for row in range(N):
        for col in range(N):
            energy+=(-g[row,col]*J*sum(near(g,row,col,N)))
    return energy/2


#CALCULATES THE ENERGY DIFF EQ 7.14 OF PROF HUTZLER NOTES
def e_diff (g, x ,y,N):
    sel=g[y][x]
    diff=(-2.*J*sel*sum(near (g, x ,y,N)))+h
    
    return diff

 
#GIVEN INITIAL LATTICE AND SELECTED SPIN,
#RETURNS LATTICE EITHER UPDATED OR NOT
def Ising(g, x,y,N,T):
#Performing sweeps choosing random spin and flipping or not
    b=1./(kB*T)
    
    dif = e_diff (g, x ,y,N) #CALCULATES ENERGY DIFF
    #IF SELECTED SPIN WAS TO BE FLIPPED
    
    if dif >= 0:
            g[y][x]=-g[y][x] #Si=-Si
            
       #     print "flipped"
        
    if dif < 0: 
            r=random.random()   #gives random number between 1 - 0
    
            P_flip= math.exp(b*dif)   #prob of flipping 
    
    
            if r < P_flip:
                g[y][x]=-g[y][x] #Si=-Si
          #      print "prob flipped"
          #  else:
          #      print "not flipped"
      
    return g #"diff=",dif
    
################################################################################
