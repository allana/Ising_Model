#!/usr/bin/env python

#IMPORTING RELEVANT PROGRAMMES
import numpy as np
import random
import matplotlib.pyplot as plt
import deff_1d #Imports previously written 1d Ising equations

#####CONTROLS################
sweeps=10000000      #no. of sweeps to reach  eq. 
sweeps_after=5000000 #no of sweeeps after eq. has been reached
T = np.arange(0.1, 0.7, .05)  #select temp range and increment
N_array=[500,750]


#STATING CONSTANTS
kB=1.   # Boltzman Constant J/K
J=1     #Exchange Energy
h=0     #External Field


colo='blue','red','green','black'
s=-1

for N in N_array:
    s=s+1
    
    #######SWEEPING THROUGH LATTICE############
    i=0 #sweep
    
    #Assinging Empty Arrays
    mag_T=[]
    mag_T1=[]
    T1=[]
    mag_arr=[]
    
    for var in T:
            print var
    
            g1=deff_1d.init_spin_array(N)
            
            #reaching equilibrium. results in the final lattice at eq. 
            for i in range (sweeps):
                    i=i+1
                
                
                    x_r=random.randint(0,N-1) #MAKES RANDM NUMBERS 
                #    y_r=random.randint(0,N-1) #WITH VALUES BETWEEN 0 AND N-
                
                    g1=deff_1d.Ising(g1,x_r,N,var) #updates lattice with new flipped spin or same          
            
            g_eq=g1
            
            i=0
            mag_arr=[]
            
            #Calculating the magnetizition after eq. has been reached
            for i in range (sweeps_after):
                
                i=i+1
                
                x_r=random.randint(0,N-1) #MAKES RANDM NUMBERS 
          #      y_r=random.randint(0,N-1) #WITH VALUES BETWEEN 0 AND N-
                
                g_eq=deff_1d.Ising(g_eq,x_r,N,var) #updates lattice with new flipped spin or same
                
                
                
                mag1=(np.mean(g_eq)) #calculates magnetism  NNNBBBB NOT (absolute)
    #should make one below not absolute too if make a fit to calculate T_crit
             
                
                mag_arr.append(mag1)   #makes array of magnetisim for eeach iteration
                
                
            
       #     print "mean mag=", np.mean(mag_arr), var
            mean_mag= abs(np.mean(mag_arr)) #Calculates mean mag oce equilibrium is reached  NNNNBBBBBB IS NOW ABSOLUTE
            mag_T1.append(mean_mag)   #makes array of all of these mean magetisms for each temp 
            T1.append(var)
          #  print mag_T1
            
    
        
    
        


    plt.title('1D: It. before eq.=%s  It. after=%s'%(sweeps, sweeps_after) )
    plt.xlabel('Temperature [$J/kB$]')
    plt.ylabel('Absolute |<Magnetism>| per spin [$\mu$]')
    plt.ylim(-0.1,1.1)
    plt.plot(T1, mag_T1, "x",  color=colo[s],label=N)
    plt.legend(loc='upper right')


plt.show()

