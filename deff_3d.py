#!/usr/bin/env python3



import numpy as np
import random
import math




kB=1.   # Boltzman Constant J/K
J=1.    # Exchange Energy
h=0


############Definitions###############
    
 #####3d######   
def init_spin_array(rows, cols,d):
    return np.random.choice((-1, 1), size=(rows, cols,d))



def all_spins_up(rows, cols,d):
    return np.ones((rows, cols,d))
 

#Given spin Si at x,y in lattice, prints values of nearest neighbours
#in convenion left, right, above, below
def near (g, x ,y,d,N):
    left=g[y][x-1][d] if x > 0 else g[y][N-1][d]
    right=g[y][x+1][d] if(x) <  (N-1) else g[y][0][d]
    above=g[y-1][x][d] if (y) > 0 else g[N-1][x][d]
    below=g[y+1][x][d] if (y)<(N-1) else g[0][x][d]
    
    front=g[x][y][d-1] if (d) > 0 else g[y][x][N-1]
    behind=g[x][y][d+1]  if (d)<(N-1) else g[y][x][0]
    
    return left,right,above,below,front,behind  
    
#Energy of lattice in its current state g
def current_energy(g,N):
    energy=0
    for row in range(N):
        for col in range(N):
            for dim in range(N):
                energy+=(-g[row,col,dim]*J*sum(near (g, row, col, dim,N)))
    return energy/2



#CALCULATES THE ENERGY DIFF fliiping spin EQ 7.14 OF PROF HUTZLER NOTES
def e_diff (g, x ,y,d,N):
    sel=g[y][x][d]
    diff=(-2.*J*sel*sum(near (g, x ,y,d,N)))+h
    
    return diff


    
#GIVEN INITIAL LATTICE AND SELECTED SPIN,
#RETURNS LATTICE EITHER UPDATED OR NOT    
def Ising(g, x,y,d,N,T):
#Performing sweeps choosing random spin and flipping or not
    b=1./(kB*T)
    
    dif = e_diff(g, x ,y,d,N) #CALCULATES ENERGY DIFF
    #IF SELECTED SPIN WAS TO BE FLIPPED
    
    if dif >= 0:   
             g[y][x][d]=-g[y][x][d] #Si=-Si
            
       #     print "flipped"
        
    if (dif < 0):
            r=random.random()   #gives random number between 1 - 0
    
            P_flip= math.exp(b*dif)   #prob of flipping 
           
    
    
            if r < P_flip:
                g[y][x][d]=-g[y][x][d] #Si=-Si
          #      print "prob flipped"
          #  else:
          #      print "not flipped"
      
    return g 
    
###########################################################################
