#!/usr/bin/env python3



#IMPORTING RELEVANT PROGRAMMES
import numpy as np
import random
import matplotlib.pyplot as plt
import deff_2d #Imports previously written 2d Ising equations


#####CONTROLS################
sweeps=1500000        #no. of sweeps to reach  eq. 
sweeps_after=500000   #no of sweeeps after eq. has been reached
T = np.arange(1.5, 3.8, .05)  #select temp range and increment



#STATING CONSTANTS
kB= 1.   # Boltzman Constant J/K
J=1.     #Exchange Energy
N=24     #NUMBER OF ELEMESTS PER SIDE OF LATTICE
h=0      #External Field



#######SWEEPING THROUGH LATTICE############
i=0 #sweep


#Initalsing empty arrays
sus1=[]
T1=[]



for var in T:
        print (var,"end3.8")
        g1=deff_2d.all_spins_up(N,N)
        
        
        #Initalsing empty arrays  
        mag_iter=[]     # empty array for mag values for with sweep_after values
        mag_iter_sq=[]  # empty array for mag^2 values for with sweep_after values

        
        #reaching equilibrium. results in the final lattice at eq. 
        for i in range (sweeps):
                i=i+1
            
            
                x_r=random.randint(0,N-1) #MAKES RANDM NUMBERS 
                y_r=random.randint(0,N-1) #WITH VALUES BETWEEN 0 AND N-
            
                g1=deff_2d.Ising(g1,x_r,y_r,N,var) #updates lattice with new flipped spin or same          
        
        g_eq=g1    #Making inital array the final array of sweeps before equilibium reached
                   # from previous for loop
        
        i=0        #initialing i as 0 again as use i for iterations after eq. reached below
        
       
    
        #Calculating the magnetizition after eq. has been reached
        for i in range (sweeps_after):

            i=i+1   #iterates sweeep
            
            
            x_r=random.randint(0,N-1) #MAKES RANDM NUMBERS 
            y_r=random.randint(0,N-1) #WITH VALUES BETWEEN 0 AND N-
            
            g_eq=deff_2d.Ising(g_eq,x_r,y_r,N,var) #updates lattice with new flipped spin or same     
   
                
                
            mag=abs(np.sum(g_eq)) #calculates magnetism (absolute)
            mag_sq=mag**2.       #calculates magnetism^2 (absolute)
        
            mag_iter.append(mag) # gives array of mag values for with sweep_after values                       
            mag_iter_sq.append(mag_sq)# gives array of mag^2 values for with sweep_after values

            if i == (sweeps_after-1):#for final sweep at each temperature
            #calculates susceptibility
                
                mag=abs(np.sum(mag_iter))#gets the sum of the list of magnetism of lattice for each itertion of sweeps_after
		
                magnetization=(mag/(sweeps_after*N*N))
                

                magsus=abs(np.sum(mag_iter_sq)) #sum of <m^2>                                  #Summing over all values in mag squared list
                magsusmean=(magsus/(N*N*sweeps_after)) #mean of <m^2>                      #Fisning mean of this value 
                susceptibility=(1./(kB*var))*(magsusmean-(magnetization*(mag/sweeps_after)))	
                #Above line calculates susceptibility 
                
   
	    
        sus1.append(susceptibility) #collects sus. value for each temp      
        T1.append(var)              #Collects each temperature values
	#mag1_list.append(magers)  
           



#Plotting Temperature vs Susceptibility
fig1=plt.figure()
plt.title('2D: It before Eq.=%s It. after Eq=%s.,  %s X %s'%(sweeps,sweeps_after,N,N) )
plt.xlabel('Temperature [$J/kB$]')
plt.ylabel(' Magnetic Susceptibility [$\mu$/kB]')
plt.plot(T1, sus1, "x",  color="BLUE")

#scipy.optimize.curve_fit(f, T2, sus_T2, p0=None, sigma=None, **kw)


plt.show()
