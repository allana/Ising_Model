#!/usr/bin/env python3


import numpy as np
import random
import math
import matplotlib.pyplot as plt




kB=1.   # Boltzman Constant J/K
J=1.    # Exchange Energy
h=0


############Definitions###############
def one_d_lattice_plot(ar):
          
          
    i=0
    for i in range(len(ar)):
        i=i+1
        
        if ar[i-1]==-1:
         
             plt.plot(i-1,0,'|',ms = 240,color="red") 
             plt.axis('off')
    
            
            
        if ar[i-1]==1:
          
             plt.plot(i-1,0,'|',ms = 240,color="blue") 
             plt.axis('off')
            
    return plt.show()

    
def init_spin_array(rows):
    return np.random.choice((-1, 1), size=(rows))
    
def all_spins_up(rows):
    return np.ones((rows))
 

#Given spin Si at x,y in lattice, prints values of nearest neighbours
#in convenion left, right, above, below
def near (g, x ,N):
    left=g[x-1] if x > 0 else g[N-1]
    right=g[x+1] if(x) <  (N-1) else g[0]
    
    
    return left,right  
    
#Energy of lattice in its current state g
def current_energy(g,N):
    energy=0
    for x in range(N):
        
            energy+=(-g[x]*J*sum(near(g,x,N)))
    return energy/2


#CALCULATES THE ENERGY DIFF EQ 7.14 OF PROF HUTZLER NOTES
def e_diff (g, x ,N):
    sel=g[x]
    diff=(-2.*J*sel*sum(near (g, x ,N)))+h
    
    return diff

  
def P_flip (g,x,N,T):
    sel=g[x]
    eq2=np.exp((e_diff(g, x ,sel,N))*(1./(kB*T)))
    
    return eq2,
    

    
#GIVEN INITIAL LATTICE AND SELECTED SPIN,
#RETURNS LATTICE EITHER UPDATED OR NOT    
def Ising(g, x,N,T):
#Performing sweeps choosing random spin and flipping or not
    b=1./(kB*T)
    
    dif = e_diff (g, x ,N) #CALCULATES ENERGY DIFF
    #IF SELECTED SPIN WAS TO BE FLIPPED
    
    if dif >= 0:
            g[x]=-g[x] #Si=-Si
            
       #     print "flipped"
        
    if dif < 0: 
            r=random.random()   #gives random number between 1 - 0
    
            P_flip= math.exp(b*dif)   #prob of flipping 
    
    
            if r < P_flip:
                g[x]=-g[x] #Si=-Si
          #      print "prob flipped"
          #  else:
          #      print "not flipped"
      
    return g #"diff=",dif
    
########################################################