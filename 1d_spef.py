#!/usr/bin/env python

#IMPORTING RELEVANT PROGRAMMES
import numpy as np
import random
import matplotlib.pyplot as plt
import deff_1d #Imports previously written 1d Ising equations

#####CONTROLS################
sweeps=30#0000       #no. of sweeps to reach  eq. 
sweeps_after=30#0000  #no of sweeeps after eq. has been reached
T = np.arange(0.01, 3, .05)  #select temp range and increment



#STATING CONSTANTS
kB=1.   # Boltzman Constant J/K
J=1.     #Exchange Energy
N=32     #NUMBER OF ELEMESTS PER SIDE OF LATTICE
h=0     #External Field




#######SWEEPING THROUGH LATTICE############
i=0 #sweep

#Initalsiing empty arrays
T1=[]                   #Temperature
energy_array=[]         #Energy
Specific_heat_array=[]  #Specifc heat



for var in T:
        print (var,"end7 before",sweeps,"after",sweeps_after, "N=",N)
        
        g1=deff_1d.all_spins_up(N)
        
        
        #Initalsiing empty arrays
        e_array=[]     #gives array of energy for each iteration of 'sweeps_after'
        e_sq_array=[]  #gives array of energy^2 for each iteration of 'sweeps_after'
 
 
        
        #reaching equilibrium. results in the final lattice at eq. 
        for i in range (sweeps):
                i=i+1
            
            
                x_r=random.randint(0,N-1) #MAKES RANDM NUMBERS 
                
            
                g1=deff_1d.Ising(g1,x_r,N,var) #updates lattice with new flipped spin or same          
        
        g_eq=g1    #Making inital array the final array of sweeps before equilibium reached
                   # from previous for loop
        
        i=0        #initialing i as 0 again as use i for iterations after eq. reached below
        
       
    
        #Calculating the magnetizition after eq. has been reached
        for i in range (sweeps_after):

            i=i+1   #iterates sweeep
            
            
            x_r=random.randint(0,N-1) #MAKES RANDM NUMBERS 
            y_r=random.randint(0,N-1) #WITH VALUES BETWEEN 0 AND N-
            
            g_eq=deff_1d.Ising(g_eq,x_r,N,var) #updates lattice with new flipped spin or same     


            e=deff_1d.current_energy(g_eq,N)  # current (each sweep) energy of lattice 
            e_sq=e**2                   # current energy of lattice squared
            
            e_array.append(e)         #gives array of energy for each iteration of 'sweeps_after' 
            e_sq_array.append(e_sq)     #gives array of energy^2 for each iteration of 'sweeps_after' 
        
            
            
            if i == (sweeps_after-1):#for final sweep at each temperature
            #calculates the energy and specific heat
                
                
                #Uses array of energy and energy^2 obtained on final iteration
                e_array.append(e)
                e_sq_array.append(e_sq)
                
        
                sum_e=np.sum(e_array)                     # Sums all energy values for all iterations of sweeps_after
                energy=(sum_e/(sweeps_after*N))         # Mean energy per spin 

                entot=np.sum(e_sq_array)           #Sums all energy^2 values for each sweep of sweeps_after 
                enmean=(entot/(sweeps_after*N))  # gives mean value of energy^2 per spin
                
                
                specific_heat=(1/(kB*(var**2)))*(enmean-energy*(sum_e/sweeps_after))
                #Above equation calculates specific heat
        
        T1.append(var)                 #Appends temperature values
        energy_array.append(energy)         #Appends energy values
        Specific_heat_array.append(specific_heat) #Appends specific Heat values




#ENERGY V TEMPERATURE
fig1=plt.figure()
plt.title('1D: It. before eq.=%s  It. after=%s,  N=%s'%(sweeps, sweeps_after,N) )
plt.xlabel('Temperature [$J/kB$]')
plt.ylabel('Energy [J]')
plt.plot(T1, energy_array, "x",  color="BLUE")

fig1=plt.figure()

plt.title('1D: It. before eq.= %s  It. after=%s,  N=%s '%(sweeps, sweeps_after,N) )
plt.xlabel('Temperature [$J/kB$]')
plt.ylabel('Specific Heat [$J/kB^2$]' )
plt.plot(T1,Specific_heat_array , "x",  color="red")



plt.show()




